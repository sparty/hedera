﻿#!/usr/bin/env zx
'use strict';

import chalk from 'chalk';
import fs from 'fs';
require('dotenv').config()
const algoliasearch = require("algoliasearch");


const APPLICATION_ID = process.env.ALGOLIA_APPLICATION_ID;
const ADMIN_API_KEY = process.env.ALGOLIA_ADMIN_API_KEY;
const INDEX_NAME = "inventory";
const FILE_TO_UPLOAD = process.argv[3] || "data/inventory.json";

const OBJECT_ID_PREFIX = "inv_";

console.log(`✨ Attempting to sync with algolia using ${chalk.red(FILE_TO_UPLOAD)}`);

const client = algoliasearch(APPLICATION_ID, ADMIN_API_KEY);
const index = client.initIndex(INDEX_NAME);

const inventory = JSON.parse(fs.readFileSync(FILE_TO_UPLOAD));
const inventoryWithObjectIds = inventory.map(i => ({
  ...i,
  objectID: OBJECT_ID_PREFIX + i['risbn'],
  cat2: i['cat'] + " > " + i['cat2']
}));



const {objectIDs} = await index.partialUpdateObjects(inventoryWithObjectIds, {
  createIfNotExists: true
});

console.log(`✨ Updates are processing for  ${chalk.red(objectIDs.length)} items of a total ${chalk.red(inventoryWithObjectIds.length)}`);
