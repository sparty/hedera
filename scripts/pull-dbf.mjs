﻿#!/usr/bin/env zx
'use strict';

import chalk from 'chalk';
import fs from 'fs';
import { exit } from 'process';

require('dotenv').config()

const dropboxV2Api = require('dropbox-v2-api');

const DROPBOX_ACCESS_TOKEN = process.env.DROPBOX_ACCESS_TOKEN;
const DROPBOX_FILE_LOCATION = process.env.DROPBOX_FILE_LOCATION;
const OUTPUT_FILE = 'data/DatStock.dbf';

const dropbox = dropboxV2Api.authenticate({
  token: DROPBOX_ACCESS_TOKEN
});

console.log(`✨ Attempting to download ${chalk.red(DROPBOX_FILE_LOCATION)} from Dropbox and save it to ${chalk.red(OUTPUT_FILE)}`);

dropbox({
  resource: 'files/download',
  parameters: {
      'path': DROPBOX_FILE_LOCATION
  }
}, (err, result, response) => {
  if (err) { return console.log(err); }
  console.log(result);
})
.pipe(fs.createWriteStream(OUTPUT_FILE));
