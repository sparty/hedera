﻿#!/usr/bin/env zx
'use strict';

import chalk from 'chalk';
import fs from 'fs';
const {DBFFile} = require('dbffile');
const cliProgress = require('cli-progress');

const BLOCKED_ISBNS = ['TWODOLLAR', 'SPANISH', 'KIDSBOOKS', '2FRENCH', '978178663173R'];
const BLOCKED_CATEGORIES = ['STUFF', 'SUPPORT', 'RENTAL', 'STUFF-', 'MAGAZINE']
const FILE_TO_INGEST = process.argv[3] || "data/DatStock.dbf";
const FILE_TO_OUTPUT = process.argv[4] || "data/inventory.json";


const getRealIsbn = (isbn) => {
  if(isbn.indexOf("978") !== 0 || isbn.length < 13){
    return isbn;
  }
  var s = isbn.split('');
  var total = 0;
  for (var i = 0; i < 12; i++){
    total += (i % 2 === 0 ? parseInt(s[i]) : 3 * parseInt(s[i]));
  }
  return isbn.substring(0, 12) + "" + (10 - (total % 10));
}

console.log(chalk.blue(`✨ Starting ingest from ${chalk.red(FILE_TO_INGEST)}  :)`));

const dbf = await DBFFile.open(FILE_TO_INGEST);
console.log(`> File contains ${chalk.red(dbf.recordCount)} records.`);


let inventory = [];
const records = await dbf.readRecords();
const filteredRecords = records
  .filter(r => r.OH > 0 || r.BACKROOM > 0)
  .filter(r => !BLOCKED_CATEGORIES.includes(r.CAT) && !BLOCKED_ISBNS.includes(r.ISBN));

console.log(`> ${chalk.red(filteredRecords.length)} records to process (in stock and not excluded)`);

const progressBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
progressBar.start(dbf.recordCount, 0);
progressBar.update(dbf.recordCount - filteredRecords.length);

for(const record of filteredRecords){
  inventory.push({
    title: record.TITLE,
    author: record.AUTHOR,
    isbn: record.ISBN,
    cat: record.CAT,
    cat2: record.CAT2,
    price: record.PRICE,
    rcvd: record.RCVD,
    risbn: getRealIsbn(record.ISBN),
    normalizedTitle: record.TITLE.toLowerCase(),
    normalizedAuthor: record.AUTHOR.trim() === "" ? "zzzzzz" : record.AUTHOR.toLowerCase()
  });
  progressBar.increment();
}
progressBar.stop();
console.log('> Finished parsing the dBase file');
console.log(`> Writing to ${chalk.red(FILE_TO_OUTPUT)}`);

fs.writeFileSync(FILE_TO_OUTPUT, JSON.stringify(inventory, null, 2));

