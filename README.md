# hedera

hedera is the third attempt at an online inventory system for Spartacus Books. First there was inv, then there was ivy, and now there is this.

The main branch of this repo is automatically deployed to https://hedera.spartacusbooks.net.

## Running locally

To run this project locally, install the dependencies and run the local server:

```sh
yarn
yarn start
```


## Updating algolia

There are a few scripts which help update algolia. To run them you'll need to create a `.env` file or set enviroment variables.

```
ALGOLIA_APPLICATION_ID=
ALGOLIA_ADMIN_API_KEY=
DROPBOX_ACCESS_TOKEN={dropbox API access token}
DROPBOX_FILE_LOCATION={location of DatStock.dbf in dropbox}
```

And then you can run the following sequence of scripts

```sh
yarn
yarn pull-dbf
yarn ingest
yarn algolia
```

These can be run altogether as

```
yarn inventory
```
